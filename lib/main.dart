import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/auth_bloc/auth_bloc_cubit.dart';
import 'bloc/home_bloc/home_bloc_cubit.dart';
import 'bloc/obscure_text/obscure_text_cubit.dart';
import 'ui/extra/loading.dart';
import 'ui/home_bloc/home_bloc_screen.dart';
import 'ui/login/login_page.dart';
import 'utils/constant.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider<ObscureTextCubit>(
      create: (context) => ObscureTextCubit(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          scaffoldBackgroundColor: ThemeColor.kBlackColor,
        ),
        home: BlocProvider(
          create: (context) => AuthBlocCubit(),
          child: MyHomePageScreen(),
        ),
      ),
    );
  }
}

class MyHomePageScreen extends StatefulWidget {
  const MyHomePageScreen({Key? key}) : super(key: key);

  @override
  State<MyHomePageScreen> createState() => _MyHomePageScreenState();
}

class _MyHomePageScreenState extends State<MyHomePageScreen> {
  @override
  void initState() {
    Timer(const Duration(seconds: 3), () {
      context.read<AuthBlocCubit>().fetchHistoryLogin();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener<AuthBlocCubit, AuthBlocState>(
        listener: (context, state) {
          if (state is AuthBlocLoginState) {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => BlocProvider<AuthBlocCubit>(
                  create: (context) => AuthBlocCubit()..fetchHistoryLogin(),
                  child: LoginPage(),
                ),
              ),
              (route) => false,
            );
          } else if (state is AuthBlocLoggedInState) {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => BlocProvider<HomeBlocCubit>(
                  create: (context) => HomeBlocCubit()..fetchingData(),
                  child: HomeBlocScreen(),
                ),
              ),
              (route) => false,
            );
          } else {
            Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (context) => LoadingIndicator(),
              ),
              (route) => false,
            );
          }
        },
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.movie_filter_rounded,
                size: 100,
                color: ThemeColor.kRedColor,
              ),
              const SizedBox(height: 10),
              RichText(
                textScaleFactor: MediaQuery.of(context).textScaleFactor,
                text: TextSpan(
                  style: TextStyle(color: Colors.white, fontSize: 32),
                  children: <TextSpan>[
                    TextSpan(
                      text: "Move ",
                      style: TextStyle(
                        color: ThemeColor.kRedColor,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    TextSpan(
                      text: 'to Movie',
                    )
                  ],
                ),
              ),
              const SizedBox(height: 50),
              Text(
                'Welcome!',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 25,
                ),
                textAlign: TextAlign.center,
              ),
              const SizedBox(height: 15),
              Text(
                'Please Enjoy The Movie You Want!',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w600,
                  fontSize: 18,
                ),
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }
}
