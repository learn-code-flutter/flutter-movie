import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';

import '../../models/movie_model.dart';
import '../../services/api_service.dart';
import '../../utils/error_helper.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  final ApiServices? apiServices;
  HomeBlocCubit({this.apiServices}) : super(HomeBlocInitialState());

  void fetchingData() async {
    final services = apiServices ?? ApiServices();
    try {
      emit(HomeBlocLoadingState());
      MovieModel? movieResponse = await services.getMovieList();
      if (movieResponse != null) {
        emit(HomeBlocLoadedState(movieResponse.results!));
      }
    } catch (e) {
      if (e is DioError) {
        emit(HomeBlocErrorState(ErrorHelper.extractApiError(e)));
      } else {
        emit(HomeBlocErrorState(ErrorHelper.getErrorMessage(e)));
      }
    }
  }
}
