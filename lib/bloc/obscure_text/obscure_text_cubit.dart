import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'obscure_text_state.dart';

class ObscureTextCubit extends Cubit<ObscureTextState> {
  ObscureTextCubit() : super(ObscureTextState(isObscureText: true));

  void toggleObscureText(bool isObscureText) {
    emit(ObscureTextState(isObscureText: isObscureText));
  }
}
