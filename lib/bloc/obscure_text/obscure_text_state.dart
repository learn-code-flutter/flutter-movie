part of 'obscure_text_cubit.dart';

class ObscureTextState extends Equatable {
  final bool isObscureText;
  const ObscureTextState({
    required this.isObscureText,
  });

  @override
  List<Object> get props => [isObscureText];
}
