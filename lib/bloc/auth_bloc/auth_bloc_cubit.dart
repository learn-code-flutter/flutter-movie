import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../models/user.dart';
import '../../services/database_helper.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  final SharedPreferences? sharedPref;
  final DatabaseHelper? dbHelper;
  AuthBlocCubit({
    this.sharedPref,
    this.dbHelper,
  }) : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    SharedPreferences sharedPreferences =
        sharedPref ?? await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");

    debugPrint('USER : ${sharedPreferences.getString("user_value")}');
    User? user = sharedPreferences.getString("user_value")?.isNotEmpty == true
        ? User.fromJson(jsonDecode(sharedPreferences.getString("user_value")!)
            as Map<String, dynamic>)
        : null;

    if (isLoggedIn == null || user == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState(user: user));
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user) async {
    SharedPreferences sharedPreferences =
        sharedPref ?? await SharedPreferences.getInstance();
    final db = dbHelper ?? DatabaseHelper();

    emit(AuthBlocLoadingState());
    final isUserExist = await db.selectUser(user);
    if (isUserExist != null) {
      await sharedPreferences.setBool("is_logged_in", true);
      final data = isUserExist.toJson();
      sharedPreferences.setString("user_value", jsonEncode(data));
      emit(
        AuthBlocLoggedInState(
          user: isUserExist,
          message: 'Successful User Identified',
        ),
      );
    } else {
      emit(AuthBlocErrorState('Login failed, check return your input'));
    }
  }
}
