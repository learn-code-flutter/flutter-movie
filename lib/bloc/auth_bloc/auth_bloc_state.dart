part of 'auth_bloc_cubit.dart';

abstract class AuthBlocState extends Equatable {
  const AuthBlocState();

  @override
  List<Object?> get props => [];
}

class AuthBlocInitialState extends AuthBlocState {}

class AuthBlocLoadingState extends AuthBlocState {}

class AuthBlocLoggedInState extends AuthBlocState {
  final User user;
  final String? message;

  AuthBlocLoggedInState({
    required this.user,
    this.message,
  });

  @override
  List<Object?> get props => [user, message];
}

class AuthBlocLoginState extends AuthBlocState {}

class AuthBlocErrorState extends AuthBlocState {
  final error;

  AuthBlocErrorState(this.error);

  @override
  List<Object> get props => [error];
}
