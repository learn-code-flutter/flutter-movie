import 'package:dio/dio.dart';

import '../models/movie_model.dart';
import '../utils/constant.dart';
import 'dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieModel?> getMovieList({Dio? dio}) async {
    // final dio = await dioConfig.dio();
    final _dio = dio ?? await dioConfig.dio();

    String url =
        Api.baseUrl + ApiEndPoint.trendingMovie + '?api_key=' + Api.apiKey;
    // Map<String, dynamic>? queryParameters = {ApiParams.paramApiKey: Api.apiKey};

    try {
      Response response = await _dio.get(url);

      return MovieModel.fromJson(response.data);
    } catch (e) {
      if (e is DioError) {
        throw e;
      } else {
        throw Exception(e);
      }
    }
  }
}
