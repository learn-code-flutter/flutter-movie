import 'package:equatable/equatable.dart';

class MovieModel extends Equatable {
  final int page;
  final List<Results>? results;
  final int totalPages;
  final int totalResults;

  MovieModel({
    this.page = 1,
    this.results,
    this.totalPages = 0,
    this.totalResults = 0,
  });

  factory MovieModel.fromJson(Map<String, dynamic> json) {
    return MovieModel(
      page: json['page']?.toInt() ?? 0,
      results: json['results'] != null
          ? List<Results>.from(json['results'].map((x) => Results.fromJson(x)))
          : null,
      totalPages: json['total_pages']?.toInt() ?? 0,
      totalResults: json['total_results']?.toInt() ?? 0,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'page': page,
      'results': results?.map((x) => x.toJson()).toList(),
      'total_pages': totalPages,
      'total_results': totalResults,
    };
  }

  @override
  String toString() {
    return 'MovieModel(page: $page, results: $results, totalPages: $totalPages, totalResults: $totalResults)';
  }

  @override
  List<Object?> get props => [page, results, totalPages, totalResults];
}

class Results extends Equatable {
  final double? voteAverage;
  final String? overview;
  final String? releaseDate;
  final bool? adult;
  final String? backdropPath;
  final int? voteCount;
  final List<int>? genreIds;
  final int? id;
  final String? originalLanguage;
  final String? originalTitle;
  final String? posterPath;
  final String? title;
  final bool? video;
  final double? popularity;
  final String? mediaType;
  final String? name;
  final String? originalName;
  final List<String>? originCountry;
  final String? firstAirDate;

  Results({
    this.voteAverage,
    this.overview,
    this.releaseDate,
    this.adult,
    this.backdropPath,
    this.voteCount,
    this.genreIds,
    this.id,
    this.originalLanguage,
    this.originalTitle,
    this.posterPath,
    this.title,
    this.video,
    this.popularity,
    this.mediaType,
    this.name,
    this.originalName,
    this.originCountry,
    this.firstAirDate,
  });

  factory Results.fromJson(Map<String, dynamic> json) {
    return Results(
      voteAverage: json['vote_average']?.toDouble(),
      overview: json['overview'],
      releaseDate: json['release_date'],
      adult: json['adult'],
      backdropPath: json['backdrop_path'],
      voteCount: json['vote_count']?.toInt(),
      genreIds: json["genre_ids"] == null
          ? null
          : List<int>.from(json["genre_ids"].map((x) => x)),
      id: json['id']?.toInt(),
      originalLanguage: json['original_language'],
      originalTitle: json['original_title'],
      posterPath: json['poster_path'],
      title: json['title'],
      video: json['video'],
      popularity: json['popularity']?.toDouble(),
      mediaType: json['media_type'],
      name: json['name'],
      originalName: json['original_name'],
      originCountry: json["origin_country"] == null
          ? null
          : List<String>.from(json["origin_country"].map((x) => x)),
      firstAirDate: json['first_air_date'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'vote_average': voteAverage,
      'overview': overview,
      'release_date': releaseDate,
      'adult': adult,
      'backdrop_path': backdropPath,
      'vote_count': voteCount,
      'genre_ids': genreIds,
      'id': id,
      'original_language': originalLanguage,
      'original_title': originalTitle,
      'poster_path': posterPath,
      'title': title,
      'video': video,
      'popularity': popularity,
      'media_type': mediaType,
      'name': name,
      'original_name': originalName,
      'origin_country': originCountry,
      'first_air_date': firstAirDate,
    };
  }

  @override
  List<Object?> get props => [
        voteAverage,
        overview,
        releaseDate,
        adult,
        backdropPath,
        voteCount,
        genreIds,
        id,
        originalLanguage,
        originalTitle,
        posterPath,
        title,
        video,
        popularity,
        mediaType,
        name,
        originalName,
        originCountry,
        firstAirDate,
      ];

  @override
  String toString() {
    return 'Results(voteAverage: $voteAverage, overview: $overview, releaseDate: $releaseDate, adult: $adult, backdropPath: $backdropPath, voteCount: $voteCount, genreIds: $genreIds, id: $id, originalLanguage: $originalLanguage, originalTitle: $originalTitle, posterPath: $posterPath, title: $title, video: $video, popularity: $popularity, mediaType: $mediaType, name: $name, originalName: $originalName, originCountry: $originCountry, firstAirDate: $firstAirDate)';
  }
}
