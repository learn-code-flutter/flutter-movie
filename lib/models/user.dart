import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String? email;
  final String? username;
  final String? password;

  User({this.email, this.username, this.password});

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      email: json['email'],
      username: json['username'],
      password: json['password'],
    );
  }

  Map<String, dynamic> toJson() => {
        'email': email,
        'username': username,
        'password': password,
      };

  @override
  List<Object?> get props => [email, username, password];
}
