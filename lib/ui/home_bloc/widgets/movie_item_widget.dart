import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../../../models/movie_model.dart';
import '../../../utils/constant.dart';
import '../../detail_movie/detail_movie_page.dart';

class MovieItemWidget extends StatelessWidget {
  final Results data;

  const MovieItemWidget({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => DetailMoviePage(
            title: data.title ?? data.originalTitle ?? data.name ?? "No Title",
            backdropPath: data.backdropPath ?? data.posterPath!,
            releaseDate: data.releaseDate?.split('-')[0] ??
                data.firstAirDate?.split('-')[0] ??
                "No Date",
            mediaType: data.mediaType ?? "movie",
            originalLanguage: data.originalLanguage ?? "No Language",
            voteCount: data.voteCount ?? 1,
            overview: data.overview ?? "No Overview",
            popularity: data.popularity ?? 1.0,
            voteAverage: data.voteAverage ?? 1.0,
          ),
        ),
      ),
      child: Column(
        children: [
          CachedNetworkImage(
            imageUrl: Api.posterUrl + data.posterPath!,
            imageBuilder: (context, imageProvider) => Container(
              height: 250,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                image: DecorationImage(
                  image: imageProvider,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            progressIndicatorBuilder: (context, url, downloadProgress) =>
                Container(
              width: double.infinity,
              height: 250,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.white38,
                borderRadius: BorderRadius.circular(20),
              ),
              child: CircularProgressIndicator(
                value: downloadProgress.progress,
                color: ThemeColor.kRedColor,
              ),
            ),
            errorWidget: (context, url, error) => Container(
              width: double.infinity,
              height: 250,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.white38,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Icon(
                Icons.broken_image_sharp,
                color: Colors.red,
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              top: 15,
            ),
            child: Column(
              children: [
                Text(
                  data.title ?? data.originalTitle ?? data.name ?? "No Title",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w600,
                  ),
                  textDirection: TextDirection.ltr,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(height: 5),
                SizedBox(
                  width: 130,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        data.mediaType?.toUpperCase() ?? "No Type",
                        style: TextStyle(
                          color: Colors.white70,
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                        textDirection: TextDirection.ltr,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        '●',
                        style: TextStyle(
                          fontSize: 10,
                          color: Colors.grey[600],
                        ),
                      ),
                      Text(
                        data.releaseDate?.split('-')[0] ??
                            data.firstAirDate?.split('-')[0] ??
                            "No Date",
                        style: TextStyle(
                          color: Colors.white70,
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                        textDirection: TextDirection.ltr,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
