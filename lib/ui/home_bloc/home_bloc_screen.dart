import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../../common/widget/custom_button.dart';
import '../../utils/constant.dart';
import '../extra/error_screen.dart';
import '../extra/loading.dart';
import 'home_bloc_loaded_screen.dart';

class HomeBlocScreen extends StatelessWidget {
  const HomeBlocScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<HomeBlocCubit, HomeBlocState>(
      builder: (context, state) {
        if (state is HomeBlocLoadedState) {
          return HomeBlocLoadedScreen(data: state.data);
        } else if (state is HomeBlocErrorState) {
          return ErrorScreen(
            message: state.error,
            icon: (state.error == ErrorMessage.errorConnection)
                ? Icons.wifi_off_rounded
                : null,
            textColor: Colors.white,
            gap: 30,
            retryButton: SizedBox(
              height: 50,
              width: 200,
              child: CustomButton(
                text: 'Try Again',
                colorBackground: ThemeColor.kRedColor,
                onPressed: () {
                  Navigator.pop(context);

                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => BlocProvider<HomeBlocCubit>(
                        create: (context) => HomeBlocCubit()..fetchingData(),
                        child: HomeBlocScreen(),
                      ),
                    ),
                    (route) => false,
                  );
                },
              ),
            ),
          );
        } else {
          return LoadingIndicator();
        }
      },
    );
  }
}
