import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

import '../../utils/constant.dart';

class DetailMoviePage extends StatelessWidget {
  final String title;
  final String backdropPath;
  final String releaseDate;
  final String mediaType;
  final String originalLanguage;
  final double voteAverage;
  final String overview;
  final double popularity;
  final int voteCount;

  const DetailMoviePage({
    Key? key,
    required this.title,
    required this.backdropPath,
    required this.releaseDate,
    required this.mediaType,
    required this.originalLanguage,
    required this.voteCount,
    required this.overview,
    required this.popularity,
    required this.voteAverage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Stack(
            children: [
              imageMovie(backdropPath),
              customShadow(),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: IconButton(
                  onPressed: () => Navigator.pop(context),
                  icon: Icon(Icons.chevron_left, color: Colors.white, size: 32),
                ),
              ),
              infoMovie(
                title,
                releaseDate,
                mediaType,
                originalLanguage,
              ),
            ],
          ),
          const SizedBox(height: 10),
          ratingMovie(voteAverage),
          overviewMovie(overview),
          popularityAndVote(popularity, voteCount),
        ],
      ),
    );
  }

  Padding popularityAndVote(
    double popularity,
    int voteCount,
  ) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 20,
        right: 20,
        top: 30,
        bottom: 30,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          iconBadge(
            Icons.reviews_outlined,
            '$popularity',
          ),
          iconBadge(
            Icons.thumb_up_alt_outlined,
            '$voteCount',
          ),
        ],
      ),
    );
  }

  Column iconBadge(
    IconData icon,
    String text,
  ) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: Colors.white,
          size: 32,
        ),
        const SizedBox(height: 10),
        Text(
          text,
          style: TextStyle(color: Colors.white),
        ),
      ],
    );
  }

  Padding overviewMovie(String overview) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      child: Text(
        overview,
        style: TextStyle(color: Colors.white70, height: 1.5),
      ),
    );
  }

  Row ratingMovie(double voteAverage) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          '$voteAverage',
          style: TextStyle(
            fontSize: 18,
            color: Color(0xFFF7BB0E),
            fontWeight: FontWeight.w600,
          ),
        ),
        const SizedBox(width: 5),
        RatingBar.builder(
          initialRating: voteAverage,
          minRating: 1.0,
          maxRating: 10.0,
          ignoreGestures: true,
          direction: Axis.horizontal,
          allowHalfRating: true,
          itemCount: 5,
          itemSize: 24,
          unratedColor: Colors.grey,
          itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          itemBuilder: (context, _) => Icon(
            Icons.star,
            color: Colors.amber,
          ),
          onRatingUpdate: (rating) {
            print(rating);
          },
        ),
      ],
    );
  }

  Container infoMovie(
    String title,
    String releaseDate,
    String mediaType,
    String originalLanguage,
  ) {
    return Container(
      width: double.infinity,
      height: 500,
      padding: const EdgeInsets.only(bottom: 5),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            title,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 32,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 10),
          Text(
            '${releaseDate.split('-')[0]} • ${mediaType.toUpperCase()} • ${originalLanguage.toUpperCase()}',
            style: TextStyle(
              color: Colors.white70,
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }

  CachedNetworkImage imageMovie(String backdropPath) {
    return CachedNetworkImage(
      imageUrl: Api.posterUrl + backdropPath,
      imageBuilder: (context, imageProvider) => Container(
        width: double.infinity,
        height: 500,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.fill,
          ),
        ),
      ),
      placeholder: (context, url) => Container(
        width: double.infinity,
        height: 500,
        alignment: Alignment.center,
        child: CircularProgressIndicator(
          color: ThemeColor.kRedColor,
        ),
      ),
      errorWidget: (context, url, error) => Container(
        width: double.infinity,
        height: 500,
        alignment: Alignment.center,
        child: Icon(
          Icons.broken_image_sharp,
          color: Colors.red,
        ),
      ),
    );
  }

  Widget customShadow() {
    return Container(
      width: double.infinity,
      height: 421,
      margin: const EdgeInsets.only(top: 80),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Colors.white.withOpacity(0),
            ThemeColor.kBlackColor,
          ],
        ),
      ),
    );
  }
}
