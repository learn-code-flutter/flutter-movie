import 'package:flutter/material.dart';

import '../../utils/constant.dart';

class LoadingIndicator extends StatelessWidget {
  final double height;
  final Color? color;

  const LoadingIndicator({
    Key? key,
    this.height = 10,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.movie_filter_rounded,
              size: 100,
              color: ThemeColor.kRedColor,
            ),
            const SizedBox(height: 10),
            RichText(
              textScaleFactor: MediaQuery.of(context).textScaleFactor,
              text: TextSpan(
                style: TextStyle(color: Colors.white, fontSize: 32),
                children: <TextSpan>[
                  TextSpan(
                    text: "Move ",
                    style: TextStyle(
                      color: ThemeColor.kRedColor,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  TextSpan(
                    text: 'to Movie',
                  )
                ],
              ),
            ),
            const SizedBox(height: 20),
            CircularProgressIndicator(color: Colors.white),
          ],
        ),
      ),
    );
  }
}
