import 'package:flutter/material.dart';

import '../../utils/constant.dart';

class ErrorScreen extends StatelessWidget {
  final String message;
  final IconData? icon;
  final Function()? retry;
  final Color? textColor;
  final double fontSize;
  final double gap;
  final Widget? retryButton;

  const ErrorScreen({
    Key? key,
    this.message = "",
    this.icon,
    this.retry,
    this.textColor,
    this.fontSize = 14,
    this.gap = 10,
    this.retryButton,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              icon ?? Icons.error_outline,
              size: 100,
              color: ThemeColor.kRedColor,
            ),
            const SizedBox(height: 10),
            Text(
              'Ooops!',
              style: TextStyle(
                color: Colors.white,
                fontSize: 32,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 20),
            Text(
              message,
              style: TextStyle(
                fontSize: fontSize,
                height: 1.5,
                color: textColor ?? Colors.black,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: gap),
            if (retryButton == null && retry != null) ...[
              IconButton(
                onPressed: () {
                  if (retry != null) retry!();
                },
                icon: Icon(Icons.refresh_sharp),
              ),
            ] else ...[
              retryButton!,
            ]
          ],
        ),
      ),
    );
  }
}
