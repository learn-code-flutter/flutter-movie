import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../bloc/obscure_text/obscure_text_cubit.dart';
import '../../../common/widget/text_form_field.dart';

class FormRegisterWidget extends StatelessWidget {
  const FormRegisterWidget({
    Key? key,
    required this.formKey,
    required TextController usernameController,
    required TextController emailController,
    required TextController passwordController,
  })  : _usernameController = usernameController,
        _emailController = emailController,
        _passwordController = passwordController,
        super(key: key);

  final GlobalKey<FormState> formKey;
  final TextController _usernameController;
  final TextController _emailController;
  final TextController _passwordController;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomTextFormField(
            context: context,
            controller: _usernameController,
            hint: 'Enter your username',
            label: 'Username',
            textInputAction: TextInputAction.done,
          ),
          const SizedBox(height: 20),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Enter your email',
            label: 'Email',
            textInputAction: TextInputAction.done,
            validator: (val) {
              final pattern = RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null) {
                return pattern.hasMatch(val)
                    ? null
                    : 'Please enter valid email';
              }
            },
          ),
          const SizedBox(height: 20),
          BlocBuilder<ObscureTextCubit, ObscureTextState>(
            builder: (context, state) {
              return CustomTextFormField(
                context: context,
                label: 'Password',
                hint: 'Enter your password',
                controller: _passwordController,
                textInputAction: TextInputAction.done,
                isObscureText: state.isObscureText,
                suffixIcon: IconButton(
                  icon: Icon(
                    state.isObscureText
                        ? Icons.visibility_off_outlined
                        : Icons.visibility_outlined,
                  ),
                  onPressed: () {
                    context
                        .read<ObscureTextCubit>()
                        .toggleObscureText(!state.isObscureText);
                  },
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}
