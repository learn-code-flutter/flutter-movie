import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/auth_bloc/auth_bloc_cubit.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../../common/widget/custom_button.dart';
import '../../common/widget/text_form_field.dart';
import '../../models/user.dart';
import '../../services/database_helper.dart';
import '../../utils/constant.dart';
import '../home_bloc/home_bloc_screen.dart';
import 'widgets/form_register_widget.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _usernameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    void onFormSubmitted() async {
      final _username = _usernameController.value;
      final _email = _emailController.value;
      final _password = _passwordController.value;

      final db = new DatabaseHelper();

      if (formKey.currentState?.validate() == true &&
          _email.isNotEmpty &&
          _password.isNotEmpty) {
        User user = User(
          username: _username,
          email: _email,
          password: _password,
        );

        db.saveUser(user);
        context.read<AuthBlocCubit>().loginUser(user);
      }
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 100),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
              ),
              child: const Text(
                'Sign up',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 32,
                ),
              ),
            ),
            const SizedBox(height: 50),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height - 200,
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Welcome',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'Hello there, finish your registration!',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 30),
                  FormRegisterWidget(
                    formKey: formKey,
                    usernameController: _usernameController,
                    emailController: _emailController,
                    passwordController: _passwordController,
                  ),
                  const SizedBox(height: 40),
                  SizedBox(
                    width: double.infinity,
                    height: 50,
                    child: BlocConsumer<AuthBlocCubit, AuthBlocState>(
                      listener: (context, state) async {
                        if (state is AuthBlocLoggedInState) {
                          if (state.message != null) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                backgroundColor: ThemeColor.kRedColor,
                                content: Text(
                                  state.message!,
                                  style: TextStyle(color: Colors.white),
                                ),
                                duration: Duration(seconds: 1),
                              ),
                            );
                            await Future.delayed(Duration(seconds: 1));
                          }

                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BlocProvider<HomeBlocCubit>(
                                create: (context) =>
                                    HomeBlocCubit()..fetchingData(),
                                child: HomeBlocScreen(),
                              ),
                            ),
                            (route) => false,
                          );

                          FocusManager.instance.primaryFocus!.unfocus();
                          formKey.currentState?.reset();
                        } else if (state is AuthBlocErrorState) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              backgroundColor: ThemeColor.kRedColor,
                              content: Text(
                                state.error,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          );
                        }
                      },
                      builder: (context, state) {
                        if (state is AuthBlocLoadingState) {
                          return SizedBox(
                            height: 40,
                            width: 40,
                            child: Center(
                              child: CircularProgressIndicator(
                                color: Colors.red,
                              ),
                            ),
                          );
                        } else {
                          return CustomButton(
                            text: 'Sign up',
                            onPressed: onFormSubmitted,
                            colorBackground: ThemeColor.kRedColor,
                          );
                        }
                      },
                    ),
                  ),
                  const Spacer(),
                  Center(
                    child: RichText(
                      textScaleFactor: MediaQuery.of(context).textScaleFactor,
                      text: TextSpan(
                        style: TextStyle(color: Colors.grey),
                        children: <TextSpan>[
                          TextSpan(
                            text: "Already have an account? ",
                          ),
                          TextSpan(
                            text: 'Sign in',
                            style: TextStyle(
                              color: ThemeColor.kRedColor,
                              fontWeight: FontWeight.w600,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.pop(context);
                              },
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
