import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/auth_bloc/auth_bloc_cubit.dart';
import '../../bloc/home_bloc/home_bloc_cubit.dart';
import '../../common/widget/custom_button.dart';
import '../../common/widget/text_form_field.dart';
import '../../models/user.dart';
import '../../utils/constant.dart';
import '../home_bloc/home_bloc_screen.dart';
import '../register/register_page.dart';
import 'widgets/form_login_widget.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    void onFormSubmitted() async {
      final _email = _emailController.value;
      final _password = _passwordController.value;
      if (formKey.currentState?.validate() == true &&
          _email.isNotEmpty &&
          _password.isNotEmpty) {
        User user = User(
          email: _email,
          password: _password,
        );

        context.read<AuthBlocCubit>().loginUser(user);
      }
    }

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 100),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 20,
              ),
              child: const Text(
                'Sign in',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 32,
                ),
              ),
            ),
            const SizedBox(height: 50),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height - 200,
              padding: const EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20),
                  topRight: Radius.circular(20),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Welcome Back',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    'Hello there, sign to continue!',
                    style: TextStyle(
                      color: Colors.grey,
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 30),
                  FormLoginWidget(
                    formKey: formKey,
                    emailController: _emailController,
                    passwordController: _passwordController,
                  ),
                  const SizedBox(height: 40),
                  SizedBox(
                    width: double.infinity,
                    height: 50,
                    child: BlocConsumer<AuthBlocCubit, AuthBlocState>(
                      listener: (context, state) async {
                        if (state is AuthBlocLoggedInState) {
                          if (state.message != null) {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                backgroundColor: ThemeColor.kRedColor,
                                content: Text(
                                  state.message!,
                                  style: TextStyle(color: Colors.white),
                                ),
                                duration: Duration(seconds: 1),
                              ),
                            );
                            await Future.delayed(Duration(seconds: 1));
                          }

                          Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BlocProvider<HomeBlocCubit>(
                                create: (context) =>
                                    HomeBlocCubit()..fetchingData(),
                                child: HomeBlocScreen(),
                              ),
                            ),
                            (route) => false,
                          );

                          FocusManager.instance.primaryFocus!.unfocus();
                          formKey.currentState?.reset();
                        } else if (state is AuthBlocErrorState) {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              backgroundColor: ThemeColor.kRedColor,
                              content: Text(
                                state.error,
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          );
                        }
                      },
                      builder: (context, state) {
                        if (state is AuthBlocLoadingState) {
                          return SizedBox(
                            height: 40,
                            width: 40,
                            child: Center(
                              child: CircularProgressIndicator(
                                color: Colors.red,
                              ),
                            ),
                          );
                        } else {
                          return CustomButton(
                            text: 'Sign in',
                            onPressed: onFormSubmitted,
                            colorBackground: ThemeColor.kRedColor,
                          );
                        }
                      },
                    ),
                  ),
                  const Spacer(),
                  Center(
                    child: RichText(
                      textScaleFactor: MediaQuery.of(context).textScaleFactor,
                      text: TextSpan(
                        style: TextStyle(color: Colors.grey),
                        children: <TextSpan>[
                          TextSpan(
                            text: "Don't have an account? ",
                          ),
                          TextSpan(
                            text: 'Sign up',
                            style: TextStyle(
                              color: ThemeColor.kRedColor,
                              fontWeight: FontWeight.w600,
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                FocusManager.instance.primaryFocus!.unfocus();
                                formKey.currentState?.reset();
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        BlocProvider<AuthBlocCubit>(
                                      create: (context) => AuthBlocCubit(),
                                      child: RegisterPage(),
                                    ),
                                  ),
                                );
                              },
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
