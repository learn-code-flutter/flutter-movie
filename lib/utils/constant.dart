import 'package:flutter/material.dart';

class Preference {
  static const userInfo = "user-info";
}

class Api {
  static const baseUrl = "https://api.themoviedb.org/3/";
  static const posterUrl = "https://image.tmdb.org/t/p/w500/";
  static const apiKey = "6fa6d9e82280a452c8ce4a35938e9b9f";
}

class ApiEndPoint {
  static const trendingMovie = "trending/all/day";
}

class ApiParams {
  static const paramApiKey = "api_key";
}

class ThemeColor {
  static Color kRedColor = const Color(0xFFF00000);
  static Color kBlackColor = const Color(0xFF29282C);
}

class ErrorMessage {
  static const errorConnection =
      "Cannot connect to server. Make sure you have proper internet connection";
  static const otherError = "Something went wrong";
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
