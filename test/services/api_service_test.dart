import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:mockito/mockito.dart';

import '../fixtures/fixture_reader.dart';
import '../helpers/test_helper.mocks.dart';

void main() {
  late MockDio mockDio;
  late ApiServices apiServices;
  late String url;
  // late Map<String, dynamic> queryParameters;

  setUp(() {
    mockDio = MockDio();
    apiServices = ApiServices();
    url = Api.baseUrl + ApiEndPoint.trendingMovie + '?api_key=' + Api.apiKey;
    // queryParameters = {ApiParams.paramApiKey: Api.apiKey};
  });

  final tResult = [
    Results(
      voteAverage: 1.0,
      overview: "test",
      releaseDate: "2022-02-09",
      adult: false,
      backdropPath: "test",
      voteCount: 1,
      genreIds: [1, 2],
      id: 1,
      originalLanguage: "test",
      originalTitle: "test",
      posterPath: "test",
      title: "test",
      video: false,
      popularity: 1.0,
      mediaType: "test",
      name: "test",
      originalName: "test",
      originCountry: ["test"],
      firstAirDate: "2022-04-24",
    ),
  ];

  final tMovie = MovieModel(
    page: 1,
    results: tResult,
    totalPages: 1000,
    totalResults: 2000,
  );

  group(
    'get movie list from remote data',
    () {
      test('should get list of MovieModel', () async {
        //arrange
        when(mockDio.get(any)).thenAnswer(
          (_) async => Response(
            requestOptions: RequestOptions(path: url),
            data: json.decode(fixture('movie.json')),
            statusCode: 200,
          ),
        );

        //act
        final result = await apiServices.getMovieList(dio: mockDio);

        //assert
        expect(result, equals(tMovie));
      });

      test('should return DioError when reach timeout', () async {
        //arrange
        when(mockDio.get(any)).thenThrow(DioError(
          requestOptions: RequestOptions(
            path: url,
          ),
          type: DioErrorType.connectTimeout,
        ));

        //act
        final call = apiServices.getMovieList;

        //assert
        try {
          await call(dio: mockDio);
        } catch (e) {
          expect(e, isInstanceOf<DioError>());
        }
        // throwsA(
        //   DioError(
        //     requestOptions: RequestOptions(path: url),
        //     type: DioErrorType.connectTimeout,
        //   ),
        // )
        // expect(() => call(dio: mockDio), throwsA(isA<DioError>()));
      });
      
      test('should return Exception when failed to call', () async {
        //arrange
        when(mockDio.get(any)).thenThrow(Exception());

        //act
        final call = apiServices.getMovieList;

        //assert
        try {
          await call(dio: mockDio);
        } catch (e) {
          expect(e, isInstanceOf<Exception>());
        }
        // throwsA(
        //   DioError(
        //     requestOptions: RequestOptions(path: url),
        //     type: DioErrorType.connectTimeout,
        //   ),
        // )
        // expect(() => call(dio: mockDio), throwsA(isA<DioError>()));
      });
    },
  );
}
