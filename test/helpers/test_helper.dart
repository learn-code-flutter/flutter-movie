import 'package:dio/dio.dart';
import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/services/database_helper.dart';
import 'package:mockito/annotations.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqlite_api.dart';

@GenerateMocks([
  Dio,
  Database,
  DatabaseHelper,
  ApiServices,
  SharedPreferences,
])
void main() {}
