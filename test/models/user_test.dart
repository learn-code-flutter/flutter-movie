import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/user.dart';

import '../fixtures/fixture_reader.dart';

void main() {
  final tUser = User(
    username: 'test',
    email: 'test',
    password: 'test',
  );

  group(
    'convert json to model movie',
    () {
      test('should return a valid model from JSON', () {
        //arrange
        final Map<String, dynamic> jsonMap = json.decode(fixture('user.json'));

        //act
        final result = User.fromJson(jsonMap);

        //assert
        expect(result, tUser);
      });
    },
  );

  group('convert model movie to json', () {
    test('should return a json map containing proper data', () {
      //arrange
      final expectedMap = {
        "username": "test",
        "email": "test",
        "password": "test"
      };

      //act
      final result = tUser.toJson();

      //assert
      expect(result, expectedMap);
    });
  });
}
