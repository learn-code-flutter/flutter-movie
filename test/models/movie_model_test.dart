import 'dart:convert';

import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/models/movie_model.dart';

import '../fixtures/fixture_reader.dart';

void main() {
  final tResult = [
    Results(
      voteAverage: 1.0,
      overview: "test",
      releaseDate: "2022-02-09",
      adult: false,
      backdropPath: "test",
      voteCount: 1,
      genreIds: [1, 2],
      id: 1,
      originalLanguage: "test",
      originalTitle: "test",
      posterPath: "test",
      title: "test",
      video: false,
      popularity: 1.0,
      mediaType: "test",
      name: "test",
      originalName: "test",
      originCountry: ["test"],
      firstAirDate: "2022-04-24",
    ),
  ];

  final tMovie = MovieModel(
    page: 1,
    results: tResult,
    totalPages: 1000,
    totalResults: 2000,
  );

  group(
    'convert json to model movie',
    () {
      test('should return a valid model from JSON', () {
        //arrange
        final Map<String, dynamic> jsonMap = json.decode(fixture('movie.json'));

        //act
        final result = MovieModel.fromJson(jsonMap);

        //assert
        expect(result, tMovie);
      });
    },
  );

  group(
    'convert model movie to json',
    () {
      test('should return a json map containing proper data', () {
        //arrange
        final expectedMap = {
          "page": 1,
          "results": [
            {
              "vote_average": 1.0,
              "overview": "test",
              "release_date": "2022-02-09",
              "adult": false,
              "backdrop_path": "test",
              "vote_count": 1,
              "genre_ids": [1, 2],
              "id": 1,
              "original_language": "test",
              "original_title": "test",
              "poster_path": "test",
              "title": "test",
              "video": false,
              "popularity": 1.0,
              "media_type": "test",
              "name": "test",
              "original_name": "test",
              "origin_country": ["test"],
              "first_air_date": "2022-04-24"
            },
          ],
          "total_pages": 1000,
          "total_results": 2000
        };

        //act
        final result = tMovie.toMap();

        //assert
        expect(result, expectedMap);
      });
    },
  );
}
