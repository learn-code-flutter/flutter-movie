import 'dart:convert';

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/models/user.dart';
import 'package:mockito/mockito.dart';

import '../helpers/test_helper.mocks.dart';

void main() {
  late MockSharedPreferences mockSharedPreferences;
  late AuthBlocCubit authBlocCubit;
  late MockDatabaseHelper mockDatabaseHelper;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    mockDatabaseHelper = MockDatabaseHelper();
    authBlocCubit = AuthBlocCubit(
      sharedPref: mockSharedPreferences,
      dbHelper: mockDatabaseHelper,
    );
  });

  final User tUser = User(
    username: 'test',
    email: 'test',
    password: 'test',
  );

  test('initial state should AuthBlocInitialState', () {
    expect(authBlocCubit.state, AuthBlocInitialState());
  });

  group('fetchHistoryLogin', () {
    blocTest<AuthBlocCubit, AuthBlocState>(
      'emits [AuthBlocLoggedInState] when User not null.',
      build: () {
        when(mockSharedPreferences.getBool('is_logged_in')).thenReturn(true);
        when(mockSharedPreferences.getString('user_value'))
            .thenReturn(jsonEncode(tUser.toJson()));
        return authBlocCubit;
      },
      act: (bloc) => bloc.fetchHistoryLogin(),
      expect: () => <AuthBlocState>[
        AuthBlocLoggedInState(user: tUser),
      ],
    );

    blocTest<AuthBlocCubit, AuthBlocState>(
      'emits [AuthBlocLoginState] when User is null.',
      build: () {
        when(mockSharedPreferences.getBool('is_logged_in')).thenReturn(null);
        when(mockSharedPreferences.getString('user_value')).thenReturn(null);
        return authBlocCubit;
      },
      act: (bloc) => bloc.fetchHistoryLogin(),
      expect: () => <AuthBlocState>[
        AuthBlocLoginState(),
      ],
    );
    blocTest<AuthBlocCubit, AuthBlocState>(
      'emits [AuthBlocLoginState] when login is false.',
      build: () {
        when(mockSharedPreferences.getBool('is_logged_in')).thenReturn(false);
        when(mockSharedPreferences.getString('user_value')).thenReturn(null);
        return authBlocCubit;
      },
      act: (bloc) => bloc.fetchHistoryLogin(),
      expect: () => <AuthBlocState>[
        AuthBlocLoginState(),
      ],
    );
  });

  group('loginUser', () {
    blocTest<AuthBlocCubit, AuthBlocState>(
      'emits [AuthBlocLoadingState, AuthBlocLoggedInState] when User is exist.',
      build: () {
        when(mockDatabaseHelper.selectUser(tUser))
            .thenAnswer((_) async => tUser);
        when(mockSharedPreferences.setBool('is_logged_in', true))
            .thenAnswer((_) async => true);
        when(mockSharedPreferences.setString(
                'user_value', jsonEncode(tUser.toJson())))
            .thenAnswer((_) async => true);

        return authBlocCubit;
      },
      act: (bloc) => bloc.loginUser(tUser),
      expect: () => <AuthBlocState>[
        AuthBlocLoadingState(),
        AuthBlocLoggedInState(
          user: tUser,
          message: 'Successful User Identified',
        ),
      ],
    );

    blocTest<AuthBlocCubit, AuthBlocState>(
      'emits [AuthBlocLoadingState, AuthBlocErrorState] when User is not exist.',
      build: () {
        when(mockDatabaseHelper.selectUser(tUser))
            .thenAnswer((_) async => null);
        when(mockSharedPreferences.setBool('is_logged_in', null))
            .thenAnswer((_) async => true);
        when(mockSharedPreferences.setString('user_value', null))
            .thenAnswer((_) async => true);
        return authBlocCubit;
      },
      act: (bloc) => bloc.loginUser(tUser),
      expect: () => <AuthBlocState>[
        AuthBlocLoadingState(),
        AuthBlocErrorState('Login failed, check return your input')
      ],
    );
  });
}
