import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/utils/error_helper.dart';
import 'package:mockito/mockito.dart';

import '../helpers/test_helper.mocks.dart';

void main() {
  late HomeBlocCubit homeBlocCubit;
  late MockApiServices mockApiServices;

  setUp(() {
    mockApiServices = MockApiServices();
    homeBlocCubit = HomeBlocCubit(apiServices: mockApiServices);
  });

  final tResult = [
    Results(
      voteAverage: 1.0,
      overview: "test",
      releaseDate: "2022-02-09",
      adult: false,
      backdropPath: "test",
      voteCount: 1,
      genreIds: [1, 2],
      id: 1,
      originalLanguage: "test",
      originalTitle: "test",
      posterPath: "test",
      title: "test",
      video: false,
      popularity: 1.0,
      mediaType: "test",
      name: "test",
      originalName: "test",
      originCountry: ["test"],
      firstAirDate: "2022-04-24",
    ),
  ];

  final tMovie = MovieModel(
    page: 1,
    results: tResult,
    totalPages: 1000,
    totalResults: 2000,
  );

  test('initial state should be HomeInitialState or empty', () {
    expect(homeBlocCubit.state, HomeBlocInitialState());
  });

  blocTest<HomeBlocCubit, HomeBlocState>(
    'emits [HomeBlocLoadingState, HomeBlocLoadedState] when data is gotten successfully.',
    build: () {
      when(mockApiServices.getMovieList()).thenAnswer(
        (_) async => tMovie,
      );

      return homeBlocCubit;
    },
    act: (bloc) => bloc.fetchingData(),
    expect: () => <HomeBlocState>[
      HomeBlocLoadingState(),
      HomeBlocLoadedState(
        [tMovie.results![0]],
      ),
    ],
  );

  blocTest<HomeBlocCubit, HomeBlocState>(
    'emits [HomeBlocLoadingState, HomeBlocErrorState] when data cannot be obtained.',
    build: () {
      when(mockApiServices.getMovieList()).thenThrow(Exception);

      return homeBlocCubit;
    },
    act: (bloc) => bloc.fetchingData(),
    expect: () => <HomeBlocState>[
      HomeBlocLoadingState(),
      HomeBlocErrorState(ErrorHelper.getErrorMessage('error'))
    ],
  );
}
