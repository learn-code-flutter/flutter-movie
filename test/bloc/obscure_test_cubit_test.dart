import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/bloc/obscure_text/obscure_text_cubit.dart';

void main() {
  late ObscureTextCubit obscureTextCubit;

  setUp(() {
    obscureTextCubit = ObscureTextCubit();
  });

  bool toggleStatus = false;

  test('initial state should be obscure true', () {
    expect(obscureTextCubit.state.isObscureText, true);
  });

  group('toggle obscure', () {
    blocTest<ObscureTextCubit, ObscureTextState>(
      'should emits [obscure] to false when toggle on pressed.',
      build: () => ObscureTextCubit(),
      act: (bloc) => bloc.toggleObscureText(toggleStatus),
      expect: () => <ObscureTextState>[
        ObscureTextState(isObscureText: toggleStatus),
      ],
    );
  });
}
